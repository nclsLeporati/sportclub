import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { TeamService } from '@services/team.service';
import { TeamModel } from '@models/team.model';

@Component({
  selector: 'app-team-table',
  templateUrl: './team-table.component.html',
  styleUrls: ['./team-table.component.css'],
  providers: [ TeamService ]
})
export class TeamTableComponent implements OnInit {
  @Input() action: string;
  @Output() selectedTeam = new EventEmitter();
  status: string
  loading: boolean;
  message: string;
  teamList: Array<TeamModel>
  add: boolean;
  send: boolean;
  // private team;

  constructor(
    private _teamService: TeamService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.add = false;
    this.getList();
  }

  changeTeam() {
    // this.team = document.getElementById("team-select").value;
  }

  toTeam(team) {
    this._router.navigate(['/team/', team.id], {queryParams: {name: team.name}});
    window.scroll(0,0);
  }

  selectTeam() {
    let idt = document.getElementById("team-select")["value"];
    let team = this.teamList.filter(t => t.id == idt)[0];
    this.selectedTeam.emit(team);
    // this.send = true;
  }

  getList() {
    this.loading = true;
    this._teamService.getTeamsList().subscribe(
      response => {
        this.teamList = response["data"];
        this.loading = false;
      },
      error => {
        console.log(error)
      }  
    );
  }

}
