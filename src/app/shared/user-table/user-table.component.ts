import { Component, OnInit, Input, Output } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { UserService } from '@services/user.service';
import { UserModel } from '@models/user.model';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css'],
  providers: [ UserService ]
})
export class UserTableComponent implements OnInit {
  @Input() role: string;
  @Input() team: boolean;
  @Input() title: string;
  public status: string
  public loading: boolean;
  public message: string;
  public userList: Array<UserModel>

  constructor(
    private _userService: UserService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.getList();
  }

  toUser(id) {
    this._router.navigate(['/user/edit/', id]);
    window.scroll(0,0);
  }

  getList() {
    this.loading = true;
    this._userService.getUsersList(this.role, this.team).subscribe(
      response => {
        this.userList = response['data'];
        this.loading = false;
      }
    );
  }

}
