import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImagePreloadDirective } from '@directives/image-preload.directive';

import { UserTableComponent } from './user-table/user-table.component';
import { TeamTableComponent } from './team-table/team-table.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ImagePreloadDirective,
    UserTableComponent,
    TeamTableComponent
  ],
  exports: [
    ImagePreloadDirective,
    UserTableComponent,
    TeamTableComponent
  ]
})
export class SharedModule { }
