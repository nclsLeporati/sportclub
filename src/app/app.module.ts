// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './/app-routing.module';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ActivatedRouteSnapshot } from '@angular/router';

// services
import { UserService } from './services/user.service';

// Components
import { AppComponent } from './app.component';
import { DefaultComponent } from './components/default/default.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from "./guards/auth.guard";
import { LoginGuard } from "./guards/login.guard";
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { ClubEditComponent } from './components/club-edit/club-edit.component';
import { SharedModule } from '@shared/shared.module';
import { UserNewComponent } from './components/user-new/user-new.component';
import { TeamNewComponent } from './components/team-new/team-new.component';
import { UserListComponent } from './components/user-list/user-list.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DefaultComponent,
    LoginComponent,
    RegisterComponent,
    UserEditComponent,
    ClubEditComponent,
    UserNewComponent,
    TeamNewComponent,
    UserListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    AuthGuard,
    LoginGuard,
    UserService
  ],
  exports: [
    UserListComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
