import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UploadService } from '../../services/upload.service';
import { UserModel } from '../../models/user.model'
import { GLOBAL } from '../../services/global';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
  providers: [UserService, UploadService]
})
export class UserEditComponent implements OnInit {
  public loading: string;
  status: string;
  errorMesagge: string;
  user: UserModel;
  userId: number;
  newPassword: string;
  identity;
  userDefaultImage:string;

  constructor(
    private _userService: UserService,
    private _uploadService: UploadService,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    this.userDefaultImage = "assests/images/user-male.png";
    this.loading = 'show';
    this.user = new UserModel();

    this._route.params.subscribe(params => {
				this.userId = +params["id"];
				if (this.userId == 0){
					this.user = this.identity;
					this.user.club = this.identity.club;
					this.user.id = this.identity.sub;
					this.loading = 'hidden';

				}
        // else {
				// 	this.getUser(this.userId);
				// }
			});
  }

  changePassword() {
    let checked = document.getElementById("pass-check")["checked"];
    // let checked  = (<HTMLInputElement>document.getElementById("pass-check")).checked;
    let password = document.getElementsByName("password")[0];
		password["value"] = "";
		password["disabled"] = !checked;
  }

  onSubmit() {
    // if (this.user.password == this.identity.password) {
		// 	this.user.password = "";
		// }
    this.user.password = this.newPassword;

		this._userService.updateUser(this.user).subscribe(
			response => {
				this.status = response["status"];

				if(this.status != "success"){
          if (response["code"] == 409) {
            this.status = "duplicated-email";
          } else {
            this.status = "error";
            console.log(response['msg']);
          }

				} else {
					if (this.user.password == "") {
						this.user.password = this.identity.password;
					}

					if (this.userId === 0){
						this._userService.setIdentity(this.user);
					}

				}

				window.scroll(0,0);
        // window.location.href = "/user-edit/0";
			},

			error => {
				this.errorMesagge = <any>error;

				if(this.errorMesagge != null){
					console.log(this.errorMesagge);
					alert("Error en la peticion");
				}
			}

		);
  }

  public filesToUpload: Array<File>;
	public resultUpload;

	fileChangeEvent(fileInput: any) {
		console.log("Evento change lanzado");
		this.filesToUpload = <Array<File>>fileInput.target.files;

		let token = this._userService.getToken();
    let url = GLOBAL.url + "user/upload-image-user";
    document.getElementById("upload-progress-bar").setAttribute("aria-valuenow", "0");
    document.getElementById("upload-progress-bar").style.width = "0%";
		this._uploadService.makeFileRequest(token, url, ["image"], this.filesToUpload).then(
			(result: {data}) => {
				this.resultUpload = result;
        this.user.image = result.data;
        if (this.userId === 0) {
          this.identity.image = result.data;
          this._userService.setIdentity(this.identity);
          window.location.href = "/user-edit/0";
        }
			},

			(error) => {
				console.log(error);
			}
		);
	}
}
