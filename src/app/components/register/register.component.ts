import { Component, OnInit } from '@angular/core';
// import { Router, ActivatedRoute } from '@angular/router';
// import { LoginService } from '../services/login.service';
// import { ClubService } from '../services/club.service';
import { UserService } from '../../services/user.service';
import { ClubService } from '../../services/club.service';
import { ClubModel } from '../../models/club.model';
import { UserModel } from '../../models/user.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserService, ClubService]
})
export class RegisterComponent implements OnInit{
	title: string;
  step: string;
	public userErrors;
	public clubErrors;
	public status: string;
	public message: string;
	public actualSport;
  club: ClubModel;
	public user: UserModel;

	constructor(
		private _userService: UserService,
		private _clubService: ClubService
		// private _route: ActivatedRoute,
		// private _router: Router
	){}

	ngOnInit(){
		this.title = "Registrarse";
    this.step = "start"
    this.club = new ClubModel();
    this.user = new UserModel();
		// this.actualSportId = this._loginService.getActualSportId();
	}

  onSubmitClub(){
    window.scroll(0,0);
    console.log(this.club);
    this.step = "user";
  }

  onSubmitUser(){
    window.scroll(0,0);
    console.log(this.user);
    this.step = "confirm";
    // this.submitReg();
  }

	regUser(){
    document.getElementById("btnReg").setAttribute('disabled', "true");
		document.getElementById("btnAtras").setAttribute('disabled', "true");
		
		this._userService.register(this.user).subscribe(
			response => {
				this.user = response['data'];
				this.regClub(response['token']);
			},
			error => {
				this.step = "error";
				this.userErrors = error['errors'];
				console.log(error);
			}
		);
	}

	regClub(token) {
		this._clubService.register(this.club, token).subscribe(
			response => {
				this.club = response.data;
				this.step = "success";
			},
			error => {
				this.step = "error";
				this.clubErrors = error['errors'];
			}
		);

	}
}
