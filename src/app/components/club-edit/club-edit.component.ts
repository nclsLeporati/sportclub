import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { ClubService } from '../../services/club.service';
import { TeamService } from '../../services/team.service';
import { UserModel } from '../../models/user.model';
import { ClubModel } from '../../models/club.model';
import { TeamModel } from '../../models/team.model';

@Component({
  selector: 'app-club-edit',
  templateUrl: './club-edit.component.html',
  styleUrls: ['./club-edit.component.css'],
  providers: [UserService, ClubService, TeamService]
})
export class ClubEditComponent implements OnInit {
  public title: string;
  public defaultClubLogo: string;
  public status: string;
  errorMesagge: string;
  club: ClubModel;
  playersList: Array<UserModel>;
  usersList: Array<UserModel>;
  teamsList: Array<TeamModel>;

  constructor(
    private _userService: UserService,
    private _clubService: ClubService,
    private _teamService: TeamService,
    private _route: ActivatedRoute,
		private _router: Router
  ) { }

  ngOnInit() {
    this.title = "Club";
    this.defaultClubLogo = "assets/images/club-default.png";
    this.club = new ClubModel();

    this._route.params.subscribe(params => {
				this.getClub(params["name"]);
			});

    this.getTeamsList();
    this.getUsersList();
    this.getPlayersList();
  }

  getClub(clubName) {
    this._clubService.detail(clubName).subscribe(
			response => {

				if (response.status != "success"){
					console.log(response.status);
					this._router.navigate(["/index"]);

				} else {
					console.log(response.status);
					console.log(response.data);
					this.club = response.data;
					this.status = response.status;
				}
			},

			error => {
				this.errorMesagge = <any>error;

				if(this.errorMesagge != null){
					console.log(this.errorMesagge);
					alert("Error en la peticion");
				}
			}
		);
  }

  getPlayersList() {
    this._userService.getUsersList("player").subscribe(
      response => {
        if (response.status != "success"){
					console.log(response.msg);
				} else {
					this.playersList = response.data;
				}
      }
    );
  }

  getUsersList() {
    this._userService.getUsersList("user").subscribe(
      response => {
        if (response.status != "success"){
					console.log(response['msg']);
				} else {
					this.usersList = response['data'];
				}
      }
    );
  }

  getTeamsList() {
    this._teamService.getTeamsList().subscribe(
      response => {
        if (response['status'] != "success"){
					console.log(response['msg']);
				} else {
					this.teamsList = response['data'];
          console.log(this.teamsList);
				}
      }
    );
  }

  goTeam(team) {
    this._router.navigate(['/team', team.id], { queryParams: { name: team.name}, queryParamsHandling: 'merge' });
  }
}
