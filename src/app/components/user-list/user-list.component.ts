import { Component, OnInit, Input, Output } from '@angular/core';
import { UserService } from '../../services/user.service';
import { UserModel } from '../../models/user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  providers: [UserService]
})
export class UserListComponent implements OnInit {
  @Input() role: string;
  @Input() team: boolean;
  status: string
  message: string;
  userList: Array<UserModel>

  constructor(
    private _userService: UserService
  ) { }

  ngOnInit() {
    console.log(this.role);
    this.getList();
  }

  getList() {
    this._userService.getUsersList(this.role, this.team).subscribe(
      response => {
        if (response.status != "success"){
					console.log(response.msg);
				} else {
					this.userList = response.data;
				}
      }
    );
  }

}
