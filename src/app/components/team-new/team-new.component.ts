import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TeamService } from '../../services/team.service';
import { TeamModel } from '../../models/team.model';

@Component({
  selector: 'app-team-new',
  templateUrl: './team-new.component.html',
  styleUrls: ['./team-new.component.css'],
  providers: [TeamService]
})
export class TeamNewComponent implements OnInit {
  title: string;
  public team: TeamModel;
  public errors: any;
  public errorMessage: string;
	public status: string;
  public validName: boolean;

  constructor(
    private _teamService: TeamService
  ) {
    this.title="Registrar un nuevo equipo";
  }

  ngOnInit() {
    this.team = new TeamModel();
    this.validName = true;
  }

  onSubmit(form) {
    document.getElementById("btnSubmit").setAttribute('disabled', "true");
		this._teamService.create(this.team).subscribe(
			response => {
        this.team = response['data'];
        this.status = 'success';        

			},
			error => {
        this.errorMessage = error.error.message;
				this.errors = error.error.errors;
      });
      
      document.getElementById("btnSubmit").removeAttribute('disabled');
      window.scrollTo(0, 0);
  }

  checkName(){
    this._teamService.checkName(this.team.name).subscribe(
      response => {
        if (response.status != "success") {
          this.validName = false;
        } else {
          this.validName = true;
        }
        console.log(this.validName);
      }
    );
  }

}
