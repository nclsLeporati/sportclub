import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { TeamService } from '../../services/team.service';
import { UserModel } from '../../models/user.model';

@Component({
  selector: 'app-user-new',
  templateUrl: './user-new.component.html',
  styleUrls: ['./user-new.component.css'],
  providers: [UserService, TeamService]
})
export class UserNewComponent implements OnInit {
  public title:string;
	public errorMesagge:string;
	status:string;
	public message:string;
	public actualSportId:number;
	user:UserModel;
  teamsList;

	constructor(
		private _userService: UserService,
    private _teamService: TeamService,
		private _route: ActivatedRoute,
		private _router: Router
	){}

	ngOnInit(){

		this.title = "Registrar un nuevo jugador";
		this.cleanUser();
    this.getTeamList();
		// this.stats = new Club(null, null, null, null, null, null, null);
		// this.actualSportId = this._userService.getActualSportId();
	}

	onSubmit(form: NgForm){
    document.getElementById("btnSubmit").setAttribute('disabled', "true");

		this.user['password'] = this.user.run;

		this._userService.createUser(this.user).subscribe(
			response => {
				console.log(response.status);

				if(response.status != "success"){
					this.message = "error";
					this.status = "error"
					console.log(response.msg);
          document.getElementById("btnSubmit").setAttribute('disabled', "true");

				} else {
					console.log(response.msg);
					this.status = "success";
					this.message = "success";
					this.user = response.data;
          // form.reset();
					window.scroll(0,0);
				}
			},

			error => {
				this.errorMesagge = <any>error;

				if(this.errorMesagge != null){
					console.log(this.errorMesagge);
					alert("Error en la peticion");
          document.getElementById("btnSubmit").setAttribute('disabled', "true");
				}
			}
		);
	}

	cleanUser(){
    this.user = new UserModel();
    this.user.role = 'player';
    // document.getElementById("btnSubmit").removeAttribute('disabled');

	}

  getTeamList() {
    this._teamService.getTeamsList().subscribe(
      (response:any) => {
        if (response.status != "success"){
					console.log(response.msg);
				} else {
					this.teamsList = response.data;
          // document.getElementById("team").selectedIndex = "1";
          console.log(this.teamsList);
				}
      }
    );
  }

  test(user) {
    console.log(user);
  }

}
