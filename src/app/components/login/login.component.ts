import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css'],
	providers: [ UserService ]
})
export class LoginComponent implements OnInit {
	user: any;
	status: string;
	errorMessage;
	sport: any;

	constructor (
		private _userService: UserService,
		private _route: ActivatedRoute,
		private _router: Router
	) {}

	ngOnInit() {
		this.checkLogout();
		this.user = { email: '', password: ''};
		this.sport = 1;
	}

	checkLogout() {
	this._route.params.subscribe(params => {
			const logout = +params['id'];
			if (logout === 1) {
				this._userService.logout();
			}
		});
	}

	onSubmitLogin() {
		// Desactivar boton y mostrar mensaje
		const btn = document.getElementById('btnLogin');
		btn.setAttribute('disabled', 'true');
		this.status = 'success';

		// Login con Token
		this._userService.signup(this.user).subscribe(
			response => {
				console.log(response);
				this._userService.setToken(response['token']);

				// Obtener usuario
				this._userService.login().subscribe(
					// tslint:disable-next-line:no-shadowed-variable
					response => {
						this._userService.setSport(this.sport);
						this._userService.setIdentity(response['data']);
						// Redireccion
						window.location.href = '/';
					}
				);
			},
			error => {
				this.status = 'error';
				this.errorMessage = error.error.message;
				console.log(this.errorMessage);
				console.log(error);
				btn.removeAttribute('disabled');
				this.status = 'error';
			}
		);
	}

}
