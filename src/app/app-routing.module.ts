// Modules
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserService } from './services/user.service';

// Components
import { AuthGuard } from "./guards/auth.guard";
import { LoginGuard } from "./guards/login.guard";
import { DefaultComponent } from './components/default/default.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UserNewComponent } from './components/user-new/user-new.component';
import { ClubEditComponent } from './components/club-edit/club-edit.component';
import { TeamNewComponent } from './components/team-new/team-new.component';
// import { TeamDetailComponent } from './components/team-detail/team-detail.component';

const routes: Routes = [
    { path: '', redirectTo: 'index', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'index', component: DefaultComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
    { path: 'login/:id', component: LoginComponent},
    { path: 'user-edit/:id', component: UserEditComponent},
    { path: 'user-new', component: UserNewComponent},
    { path: 'user', loadChildren: './modules/user/user.module#UserModule', canActivate: [AuthGuard], data: {roles: ['user']} },
    // { path: 'club/:name', component: ClubEditComponent},
    { path: 'club', loadChildren: './modules/club/club.module#ClubModule'},
    { path: 'team', loadChildren: './modules/team/team.module#TeamModule'},
    // { path: 'team-new', component: TeamNewComponent},
    // { path: 'team/:id', component: TeamDetailComponent},
    { path: 'works', loadChildren: './modules/work/work.module#WorkModule'}

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
  providers: [ UserService ]
})

export class AppRoutingModule { }
