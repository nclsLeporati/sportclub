import { Component, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './services/user.service'
import { UserModel } from '@models/user.model';
// import { GLOBAL } from './services/global';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService]
})
export class AppComponent {
  title = 'Club App';
  public identity;
  public userDefaultImage: string;

  @Output() pasarDatos = new EventEmitter();

  constructor(
    private _userService: UserService,
    private _router: Router
  ){}

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    console.log(this.identity);
    this.userDefaultImage = 'assets/images/user-male.png';
  }

  clickItem() {
    document.getElementById("navbarApp").classList.remove("show");
  }

  emitirEvento() {
    this.pasarDatos.emit({
      'identity': this.identity
    });
  }
}
