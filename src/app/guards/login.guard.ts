import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable()
export class LoginGuard implements CanActivate {

  constructor(
    private _router: Router,
    private _userService: UserService
  ) { }

  canActivate() {
    if (this._userService.isAuth()) {
        console.log('Ya estás logueado');
        this._router.navigate(['/']);
        return false;
    }

    return true;
  }
}
