import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private _router: Router,
    // private _route: ActivatedRoute,
    private _userService: UserService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let user = this._userService.getIdentity();
    let roles = route.data["roles"];
    if (!user) {
        console.log('No estás logueado');
        this._router.navigate(['/login']);
        return false;
    }
    if (user.role != roles[0]) {
      console.log('No tienes los permisos necesarios');
      // this._router.navigate(['/']);
      return false;
    }

    return true;
  }
}
