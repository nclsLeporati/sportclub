import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClubDetailComponent } from "./club-detail/club-detail.component";

const clubRoutes: Routes = [
    {
      path: ':name',
      component: ClubDetailComponent,
      // pathMatch: 'full',
      // children: [
      //   { path: ':name', component: ClubDetailComponent }
      // ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(clubRoutes)],
  exports: [RouterModule]
})
export class ClubRoutingModule { }
