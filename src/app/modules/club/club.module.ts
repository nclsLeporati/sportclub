import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';
import { ClubRoutingModule } from './club-routing.module';
import { ClubDetailComponent } from './club-detail/club-detail.component';

@NgModule({
  imports: [
    CommonModule,
    ClubRoutingModule,
    SharedModule
  ],
  declarations: [
    ClubDetailComponent
  ]
})
export class ClubModule { }
