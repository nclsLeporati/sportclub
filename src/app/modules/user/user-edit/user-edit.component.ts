import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '@services/user.service';
import { UploadService } from '@services/upload.service';
import { UserModel } from '@models/user.model';
import { TeamModel } from '@models/team.model'
import { GLOBAL } from '@services/global';
import { MediaService } from '@app/services/media.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
  providers: [UserService, MediaService]
})
export class UserEditComponent implements OnInit {
  public loading: string;
  public status: string;
  public errorMesagge: string;
  public userErrors;
  public user: UserModel;
  // private userId: number;
  public newPassword: string;
  public identity;
  public userId: number;
  public userDefaultImage:string;
  public storage;

  constructor(
    private _userService: UserService,
    private _mediaService: MediaService,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.storage = GLOBAL.storage + 'users/avatars/';
    this.identity = this._userService.getIdentity();
    this.userDefaultImage = "assets/images/user-male.png";
    this.loading = 'show';
    this.user = new UserModel();    

    this._route.params.subscribe(params => {
		    this.userId = +params["id"];
        this.getUser(this.userId);
		});
  }

  getUser(id) {
    if (id == 0 || id === this.identity.id){      
      id = this.identity.id;
    }

    this._userService.getUser(id).subscribe(
      response => {
        this.user = response.data;
      },
      error => {
          alert(error.error['message']);
      }
    );
    
    this.loading = 'hidden';
  }

  updateUser() {
    this.user.password = this.newPassword;

		this._userService.updateUser(this.user).subscribe(
			response => {
        if (this.userId === 0){          
          this._userService.setIdentity(response['data']);
          this.identity = response['data'];
        }

        this.status = 'success';
			},
			error => {
        this.userErrors = error.error.errors;
        this.status = 'error';
      }
    );    
    window.scroll(0,0);
  }  

  addTeam(team) {
    let add = true
    this.user.teams.forEach(t => {
      if (t.id == team.id) {
        add = false;
      }
    });

    if (add) {
      this._userService.addTeam(this.user.id, team.id).subscribe(
        response => {
          // this.user = response["data"];
          this.user.teams.push(team);
          console.log("Equipo agregado");
          console.log(this.user.teams);
          // document.getElementById("team-select").collapse('hide');
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  removeTeam(team) {
    if (confirm("Desea remover a "+this.user.name+" del equipo "+team.name+"?")){

    this._userService.removeTeam(this.user.id, team.id).subscribe(
      response => {
        console.log("Equipo removido de usuario");
        this.user.teams.splice(this.user.teams.findIndex(x => x.id === team.id), 1)
      }
    );
    }
  }

  changePassword() {
    let checked = document.getElementById("pass-check")["checked"];
    let password = document.getElementsByName("password")[0];
		password["value"] = "";
		password["disabled"] = !checked;
  }

  public filesToUpload: Array<File>;
	public resultUpload;

	fileChangeEvent(fileInput: any) {
		this.filesToUpload = <Array<File>>fileInput.target.files;

    document.getElementById("upload-progress-bar").setAttribute("aria-valuenow", "0");
    document.getElementById("upload-progress-bar").style.width = "0%";

    let options =  {
      'id': this.user.id.toString(2),
      'table': 'users',
      'prefix': 'user_image',
      'storage': 'avatars',
      'type': 'image'
    }

		this._mediaService.makeFileRequest(options, ["image"], this.filesToUpload).then(
			(result: {data}) => {
				this.resultUpload = result;
        this.user.image = result.data;
        
        if (this.user.id === this.identity.id) {
          this.identity.image = result.data;
          this._userService.setIdentity(this.identity);
          // window.location.href = "/user/edit/0";
        }
			},

			(error) => {
				console.log(error);
			}
		);
	}

}
