import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserNewComponent } from './user-new/user-new.component';
import { UserEditComponent } from './user-edit/user-edit.component';

const userRoutes: Routes = [
  // {
  //   // path: '',
  //   // redirectTo: 'new',
  //   // pathMatch: 'full',
  //   // // children: [
  //   // //   { path: 'new', redirectTo: 'new/player' },
  //   // //   { path: 'new/:role', component: UserNewComponent },
  //   // //   { path: 'edit', redirectTo: 'edit/0' },
  //   // //   { path: 'edit/:id', component: UserEditComponent }
  //   // // ]
  // },
  { path: '', redirectTo: 'edit/0' },
  { path: 'new', redirectTo: 'new/player' },
  { path: 'new/:role', component: UserNewComponent },
  { path: 'edit', redirectTo: 'edit/0' },
  { path: 'edit/:id', component: UserEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
