import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UserService } from '@services/user.service';
import { TeamService } from '@services/team.service';
import { UserModel } from '@models/user.model';

@Component({
  selector: 'app-user-newp',
  templateUrl: './user-new.component.html',
  styleUrls: ['./user-new.component.css'],
  providers: [UserService, TeamService]
})
export class UserNewComponent implements OnInit {
	public errors: any;
	public status:string;
	public message:string;
	public actualSportId:number;
	public user:UserModel;
  public teamsList;
  public role: string;
  public roleName: string;

	constructor(
		private _userService: UserService,
    private _teamService: TeamService,
		private _route: ActivatedRoute,
		private _router: Router
	){}

	ngOnInit(){
    this.user = new UserModel();
    // this.role = this._route.snapshot.queryParams.role;
    this._route.params.subscribe(params => {
      this.user.role = params['role'];
      this.roleName = (this.user.role==='user') ? 'Entrenador' : 'Jugador';
      window.scroll(0,0);
    });
    this.getTeamList();
	}

	onSubmit(form: NgForm){
    document.getElementById("btnSubmit").setAttribute('disabled', "true");

		this.user['password'] = this.user.run;

		this._userService.createUser(this.user).subscribe(
			response => {
				console.log(response);
					this.status = "success";
					this.message = "success";
					this.user = response['data'];
					// form.reset();								
			},

			error => {
				this.errors = error.error.errors;
				this.message = "error";
				this.status = "error";
				console.log(error);				
			});

			document.getElementById("btnSubmit").removeAttribute('disabled');
			window.scroll(0,0);
	}

  getTeamList() {
    this._teamService.getTeamsList().subscribe(
      (response:any) => {
				this.teamsList = response['data'];
				// document.getElementById("team").selectedIndex = "1";
      }
    );
  }

  test(user) {
    console.log(user);
  }

}
