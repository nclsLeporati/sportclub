import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkNewComponent } from './work-new/work-new.component';
import { WorkDetailComponent } from './work-detail/work-detail.component';
import { WorkListComponent } from './work-list/work-list.component';
import { WorkEditComponent } from './work-edit/work-edit.component';


const workRoutes: Routes = [
  { path: 'new', component: WorkNewComponent },
  { path: ':id', component: WorkDetailComponent },
  { path: '', component: WorkListComponent },
  { path: ':id/edit', component: WorkEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(workRoutes)],
  exports: [RouterModule]
})
export class WorkRoutingModule { }
