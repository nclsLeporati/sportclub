import { Component, OnInit } from '@angular/core';
import { WorkModel } from '@app/models/work.model';
import { WorkService } from '@app/services/work.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-work-edit',
  templateUrl: './work-edit.component.html',
  styleUrls: ['./work-edit.component.css']
})
export class WorkEditComponent implements OnInit {
  public work: WorkModel;
  public status: string;
  public errors: any;

  constructor(
    private _workService: WorkService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this._route.params.subscribe(params => {
      this.getWork(params["id"]);
    });
  }

  getWork(id) {
    this._workService.get(id).subscribe(
      response => {
        this.work = response['data'];        
      }
    );
  }

  onSubmit() {
    this._workService.update(this.work).subscribe(
      response => {
        this.work = response['data'];
        this.status = 'success';
        this._router.navigate(['/works',this.work.id]);
      },
      
      error => {
        this.errors = error.error.errors;
        this.status = 'error';
      }
    ) 
  }
}
