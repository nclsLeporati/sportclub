import { Component, OnInit } from '@angular/core';
import { WorkModel } from '@app/models/work.model';
import { WorkService } from '@app/services/work.service';

@Component({
  selector: 'app-work-new',
  templateUrl: './work-new.component.html',
  styleUrls: ['./work-new.component.css']
})
export class WorkNewComponent implements OnInit {
  public work: WorkModel;
  public status: string;
  public errors: any;

  constructor(
    private _workService: WorkService,
  ) { }

  ngOnInit() {
    this.work = new WorkModel();
    this.status = 'create';
  }

  onSubmit() {
    this._workService.create(this.work).subscribe(
      response => {
        this.work = response['data'];
        this.status = 'success';
      },
      
      error => {
        this.errors = error.error.errors; 
        this.status = 'error';
        console.log(error);
      }
    ) 
  }

}
