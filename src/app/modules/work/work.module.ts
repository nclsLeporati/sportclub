import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkRoutingModule } from './work-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WorkNewComponent } from './work-new/work-new.component';
import { WorkService } from '@app/services/work.service';
import { WorkDetailComponent } from './work-detail/work-detail.component';
import { WorkListComponent } from './work-list/work-list.component';
import { WorkEditComponent } from './work-edit/work-edit.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    WorkRoutingModule
  ],
  declarations: [
    WorkNewComponent,
    WorkDetailComponent,
    WorkListComponent,
    WorkEditComponent
  ],
  providers: [
    WorkService
  ]
})
export class WorkModule { }
