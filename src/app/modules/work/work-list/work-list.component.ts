import { Component, OnInit } from '@angular/core';
import { WorkModel } from '@app/models/work.model';
import { WorkService } from '@app/services/work.service';

@Component({
  selector: 'app-work-list',
  templateUrl: './work-list.component.html',
  styleUrls: ['./work-list.component.css']
})
export class WorkListComponent implements OnInit {
  public works: Array<WorkModel>;  

  constructor(
    private _workService: WorkService
  ) { }

  ngOnInit() {
    this.getWorks();
  }

  getWorks() {
    this._workService.list().subscribe(
      response => {
        this.works = response['data'];
      },
      error => {
        console.log(error);
      }
    );
  }

}
