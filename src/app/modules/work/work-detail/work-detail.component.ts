import { Component, OnInit } from '@angular/core';
import { WorkModel } from '@app/models/work.model';
import { WorkService } from '@app/services/work.service';
import { ActivatedRoute } from '@angular/router';

import { MediaService } from '@services/media.service';
import { GLOBAL } from '@app/services/global';

@Component({
  selector: 'app-work-detail',
  templateUrl: './work-detail.component.html',
  styleUrls: ['./work-detail.component.css'],
  providers: [MediaService]
})
export class WorkDetailComponent implements OnInit {
  public work: WorkModel;
  public status: string;
  public images;
  public videos;
  public storage;

  constructor(
    private _route: ActivatedRoute,
    private _workService: WorkService,
    private _mediaService: MediaService
  ) { }

  ngOnInit() {
    this.storage = GLOBAL.storage;
    this._route.params.subscribe(params => {
      this.getWork(params["id"]);
    });
  }

  getWork(id) {
    this._workService.get(id).subscribe(
      response => {
        this.work = response['data'];    
        this.getMedia(this.work.id, 'image');    
      }
    );
  }

  getMedia(id:number, type:string) {
    console.log(id,type);
    this._mediaService.getMedia(id, type).subscribe(
      response => {        
        if (response['data'].length != 0) {
          this.images = response['data'];
          console.log(response);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  onDelete() {
    if (window.confirm("Desea eliminara el entrenamiento "+this.work.name+"?")) {
      this._workService.delete(this.work.id).subscribe(
        response => {
          this.status = 'deleted';
        },
        error => {
          window.alert('No ha sido posible eliminar el entrenamiento, vuelva a intentarlo.')
        }
      );
    }
  }

}
