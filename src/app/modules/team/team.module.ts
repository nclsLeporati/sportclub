import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TeamRoutingModule } from './team-routing.module';
import { TeamNewComponent } from './team-new/team-new.component';
import { TeamService } from '@services/team.service';
import { SharedModule } from '@shared/shared.module';
import { TeamDetailComponent } from './team-detail/team-detail.component';
import { MediaService } from '@app/services/media.service';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    TeamRoutingModule
  ],
  declarations: [
    TeamNewComponent,
    TeamDetailComponent
  ],
  providers: [
    TeamService,
    MediaService
  ]
})
export class TeamModule { }
