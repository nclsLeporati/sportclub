import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TeamService } from '@services/team.service';
import { TeamModel } from '@models/team.model';
import { GLOBAL } from '@app/services/global';

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css'],
  providers: [TeamService]
})
export class TeamDetailComponent implements OnInit {
  public team: TeamModel;
  public name: string;
  public status: string;
  public storage;

  constructor(
    private _teamService: TeamService,
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.storage = GLOBAL.storage + 'teams/images/';
    this.name = this._route.snapshot.queryParams.name;
    console.log(this._route.snapshot);

    this._route.params.subscribe(params => {
				this.getTeam(params["id"]);
        // this.name=params['name'];
			});
  }

  getTeam(teamId) {
		this._teamService.getTeam(teamId).subscribe(
			(response:any) => {
        this.status = 'success';
        this.team = response['data'];
        console.log(this.team);
			}
		);
	}

  deleteTeam() {
    let confirm = window.confirm("Desea eliminara el equipo "+this.team.name+"?");
    if (confirm) {
      this._teamService.deleteTeam(this.team.id).subscribe(
  			(response:any) => {
  					this.status = 'deleted';
  			});
    }
  }

}
