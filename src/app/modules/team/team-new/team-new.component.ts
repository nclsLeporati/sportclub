import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TeamModel } from '@models/team.model';
import { TeamService } from '@services/team.service';
import { UploadService } from '@app/services/upload.service';
import { GLOBAL } from '@app/services/global';
import { MediaService } from '@app/services/media.service';

@Component({
  selector: 'app-team-new',
  templateUrl: './team-new.component.html',
  styleUrls: ['./team-new.component.css']
})
export class TeamNewComponent implements OnInit {
  public title: string;
  public team: TeamModel;
  public errors: any;
  public errorMessage: string;
	public status: string;
  public validName: boolean;
  public teamDefaultImage: string;
  public images: Array<File>;
  public storage: string;

  constructor(
    private _teamService: TeamService,
    private _mediaService: MediaService
  ) {
    this.title="Registrar un nuevo equipo";
  }

  ngOnInit() {
    this.storage = GLOBAL.storage + 'teams/images/';
    this.teamDefaultImage = "assets/images/team-default.png"
    this.team = new TeamModel();
    this.validName = true;
  }

  onSubmit(form) {
    document.getElementById("btnSubmit").setAttribute('disabled', "true");
		this._teamService.create(this.team).subscribe(
			response => {
        this.team = response['data'];
        this.status = 'success';
        this.uploadImage();

			},
			error => {
        this.errorMessage = error.error.message;
        this.errors = error.error.errors;   
        this.status = 'error';
        console.log(error);
      });
      
      document.getElementById("btnSubmit").removeAttribute('disabled'); 
      // document.getElementsByName("name")[0].setAttribute('disabled', 'true');
      // document.getElementsByName("description")[0].setAttribute('disabled', 'true');
      // document.getElementsByName("image")[0].setAttribute('disabled', 'true');           
      window.scrollTo(0, 0);
  }

  checkName(){
    // this._teamService.checkName(this.team.name).subscribe(
    //   response => {
    //     if (response.status != "success") {
    //       this.validName = false;
    //     } else {
    //       this.validName = true;
    //     }
    //     console.log(this.validName);
    //   }
    // );
  }

  fileChangeEvent(fileInput: any) {
    this.images = fileInput.target.files;

    // var reader = new FileReader();
    // reader.onload = (function(file) {
    //   return function (e) {
    //     let img = document.getElementById('team-image');
    //     img.src  = e.target.result;
    //     console.log(e.target.result);
    //   }
    // });
    // reader.readAsDataURL(this.image[0]);

    
  }

  uploadImage() {    
    let options =  {
      'id': this.team.id.toString(),
      'table': 'teams',
      'prefix': 'team_image',
      'storage': 'images',
      'type': 'image'
    }

		this._mediaService.makeFileRequest(options, ["image"], this.images).then(
			(result: {data}) => {
        this.team.image = result.data;
        console.log(result.data);
			},

			(error) => {
				console.log(error);
			}
		);
  }

}
