import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TeamNewComponent } from './team-new/team-new.component';
import { TeamDetailComponent } from './team-detail/team-detail.component';

const teamRoutes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'new',
  //   pathMatch: 'full',
  //   children: [
  //     { path: 'new', component: TeamNewComponent },
  //     // { path: 'new/:role', component: UserNewComponent },
  //     // { path: 'edit', redirectTo: 'edit/0' },
  //     { path: ':id', component: TeamDetailComponent }
  //   ]
  // },
  { path: 'new', component: TeamNewComponent },
  { path: ':id', component: TeamDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(teamRoutes)],
  exports: [RouterModule]
})
export class TeamRoutingModule { }
