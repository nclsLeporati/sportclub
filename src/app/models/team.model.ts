export class TeamModel {
	public id:number=0;
	public name:string="";
	public description:string="";
	public image: null;
	public createdAt=null;
	public updatedAt=null;
	public sportId:number=null;
	public creator=null;
	public club=null;
	public status:string="active";
}
