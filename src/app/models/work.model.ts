export class WorkModel {
    public id: number;
    public name: string;
    public description: string;
    public body:string;
    public time:number;
    public status:string;
    public created_at;
    public updated_at;
    public creator_id:number;
    public club_id:number;
    public sport_id:number;
  
    constructor() {
      this.id = 0,
      this.name = "",
      this.description = "",
      this.body = "",
      this.time = 0,
      this.status = "active",
      this.created_at = null,
      this.updated_at = null,
      this.creator_id = null,
      this.sport_id = 1,
      this.club_id = null
    }
  }
  