export class ClubModel {
  public id: number;
  public name: string;
  public alias: string;
  public description:string;
  public image:string;
  public origin:string;
  public city:string;
  public createdAt;
  public updatedAt;
  public creatorId:number;
  public status:string;
  public sport_id:number;

  constructor() {
    this.id = 0,
    this.name = "",
    this.alias = "",
    this.description = "",
		this.image = "",
		this.origin = "",
    this.city = "",
		this.createdAt = null,
		this.updatedAt = null,
		this.creatorId = 0,
    this.status = "to-confirm",
    this.sport_id = 1
  }
}
