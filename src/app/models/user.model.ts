export class UserModel {
	public id: number;
	public role: string;
	public run: string;
	public name: string;
	public lastname: string;
	public email: string;
	public password: string;
	public image;
	public birthDate;
	public origin: string;
	public nacionality: string;
	public club;
	public sport;
	public teams;
	public status: string;

	constructor() {
		this.id = 0;
		this. role = "user";
		this.run = "";
		this.name = "";
		this.lastname = "";
		this.email = "";
		this.password = "";
		this.image = null;
		this.birthDate = null;
		this.origin = "";
		this.nacionality = "";
		this.club = null;
		this.sport = 1;
		this.teams = null;
		this.status = "to-confirm";		
	}
}
