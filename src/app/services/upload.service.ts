import {Injectable} from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { UserService } from "./user.service";
// import { Observable } from 'rxjs/operators';

@Injectable()
export class UploadService {

	constructor(private _http: HttpClient, private _userService: UserService){}

	makeFileRequest(url: string, options, params: Array<string>, files: Array<File>){
		return new Promise((resolve, reject) => {
      document.getElementById("bar").style.display = "block";

			var formData: any = new FormData();
			var xhr = new XMLHttpRequest();

			var name_file_input = params[0];
			for (var i=0; i<files.length; i++){
				formData.append(name_file_input, files[i], files[i].name);
			}

			formData.append('auth', this._userService.getToken());
			formData.append('id', options['id']);
			formData.append('table', options['table']);
			formData.append('prefix', options['prefix']);
			formData.append('storage', options['storage']);

			xhr.onreadystatechange = function(){
				if (xhr.readyState == 4){
					if (xhr.status == 200){
						resolve(JSON.parse(xhr.response))
					} else {
						reject(xhr.response);
					}
				}
			}

			xhr.upload.addEventListener("progress", function(event: any){
				document.getElementById("upload-progress-bar").setAttribute("aria-valuenow", "0");
				document.getElementById("upload-progress-bar").style.width = "0%";

				var percent = (event.loaded/event.total) * 100;
				let prc = Math.round(percent).toString();

				// document.getElementById("upload-progress-bar").setAttribute("value", prc);
				document.getElementById("upload-progress-bar").style.width = prc + "%";
				document.getElementById("upload-progress-bar").innerHTML = Math.round(percent)+" % Subiendo archivo";
			}, false);

			xhr.addEventListener("load", function(){
				document.getElementById("upload-progress-bar").innerHTML = "Archivo subido";
				let prc = "100";
				// document.getElementById("upload-progress-bar").setAttribute("value", prc);
				document.getElementById("upload-progress-bar").setAttribute("aria-valuenow",
				document.getElementById("upload-progress-bar").style.width = prc+"%");
			}, false);

			xhr.addEventListener("error", function(){
				document.getElementById("upload-progress-bar").innerHTML = "Error al subir archivo";
			}, false);

			xhr.addEventListener("abort", function(){
				document.getElementById("upload-progress-bar").innerHTML = "Subida  abortada";
			}, false);

			xhr.open("POST", url, true);
			xhr.send(formData);
		});
	}
}
