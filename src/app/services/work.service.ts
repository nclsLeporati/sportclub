import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { GLOBAL } from './global';
import { UserService } from './user.service';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';

@Injectable()
export class WorkService {
  private url: string;
  private headers: HttpHeaders;
  
  constructor(private _http: HttpClient, private _userService: UserService) {
    this.url = GLOBAL.url + 'works';
		this.headers = new HttpHeaders({
			'Content-Type': 'application/json'
		});
  }

  headerAuth(): HttpHeaders {
		return this.headers.set('Authorization', 'Bearer ' + this._userService.getToken());
  }

  get(id): Observable<any> {
    return this._http.get(this.url + '/' + id, {headers: this.headerAuth()})
    .pipe(
      retry(3)
    );
  }

  list(): Observable<any> {
    let params = new HttpParams().set('sport_id', this._userService.getSport());
    return this._http.get(this.url, {params: params, headers: this.headerAuth()})
  }
  
  create(work): Observable<any> {
    work['sport_id'] = this._userService.getSport();
    let params = JSON.stringify(work);

    return this._http.post(this.url, params, {headers: this.headerAuth()});
  }

  update(work) {
    let params = JSON.stringify(work)

    return this._http.put(this.url + '/' + work.id, params, {headers: this.headerAuth()})
  }

  delete(id) {
    return this._http.delete(this.url + '/' + id, {headers: this.headerAuth()});
  }
}
