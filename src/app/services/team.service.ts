import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from './user.service';
import { GLOBAL } from './global';
import { TeamModel } from '../models/team.model';

@Injectable()
export class TeamService {
  private url: string;
  private identity: any;
  private token;
  private team: any;
  private headers: HttpHeaders;

  constructor(
    private _http: HttpClient,
    private _userService: UserService
  ) {
    this.url = GLOBAL.url + "teams";
    this.headers = new HttpHeaders({
			'Content-Type': 'application/json'
		});	
  }

  headerAuth(): HttpHeaders {
		return this.headers.set('Authorization', 'Bearer ' + this._userService.getToken());
	}

  create(team): Observable<any>{
    team['sport_id'] = this._userService.getSport();
    let params = JSON.stringify(team);
    console.log(team);

    return this._http.post(this.url, params, {headers: this.headerAuth()});
  }

  checkName(name): Observable<any> {
    let sportId = this._userService.getSport();
    let clubId = this._userService.getIdentity().club.id;
    return this._http.get(this.url+
      'check-name?name='+name+'&sport='+sportId+'&club='+clubId);
  }

  getTeamsList() {    
    let sportId = this._userService.getSport();
    let clubId = this._userService.getIdentity().club.id;

    // const url = this.url+'list?sport='+sportId+"&club="+clubId;
    return this._http.get(this.url, {headers: this.headerAuth()});
  }

  getTeam(id) {
		return this._http.get(this.url+"/"+id, {headers: this.headerAuth()});
  }

  deleteTeam(id){
		return this._http.delete(this.url+"/"+id, {headers: this.headerAuth()});
	}

}
