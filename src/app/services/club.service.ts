import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from './user.service';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';


@Injectable()
export class ClubService {
  private url: string;
  private headers: HttpHeaders;

  constructor(
    private _http: HttpClient,
    private _userService: UserService
  )
  {
    this.url = GLOBAL.url + "clubs";
    this.headers = new HttpHeaders({
			'Content-Type': 'application/json'
		});
  }

  headerAuth(): HttpHeaders {
		return this.headers.set('Authorization', 'Bearer ' + this._userService.getToken());
	}

  register(club, token): Observable<any> {
    let params = JSON.stringify(club);
    const headers = this.headers.set('Authorization', 'Bearer ' + token)

    return this._http.post(this.url, params, {headers: headers});
  }

  detail(clubName): Observable<any> {
    return this._http.get(this.url + '/' + clubName, {headers: this.headerAuth()});
  }

}
