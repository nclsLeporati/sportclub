import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError  } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { GLOBAL } from './global';
import { UserModel } from '../models/user.model';

@Injectable()
export class UserService {
	private url: string;
	private identity: any;
	private token: any;
	private sport: any;
	private headers: HttpHeaders;

	constructor( private _http: HttpClient, private _router: Router) {
		this.url = GLOBAL.url;
		this.headers = new HttpHeaders({
			'Content-Type': 'application/json'
		});
	}

	headerAuth(): HttpHeaders {
		return this.headers.set('Authorization', 'Bearer ' + this.getToken());
	}

	getIdentity(): any {
		const identity = JSON.parse(localStorage.getItem('identity'));
		return identity;
	}

	setIdentity(user): void {
		localStorage.setItem('identity', JSON.stringify(user));
		(user != null) ? this.identity = user : this.identity = null;
	}

	getToken() {
		const token = localStorage.getItem('token');
		if (token != null) { return token.replace(/['"]+/g, ''); }
		return null;
	}

	setToken(token): void {
		localStorage.setItem('token', JSON.stringify(token));
		(token != null) ? this.token = token : this.token = null;
	}

	getSport(): any {
		const sport = JSON.parse(localStorage.getItem('sport'));
		return sport;
	}

	setSport(sport): void {
		localStorage.setItem('sport', JSON.stringify(sport));
		(sport != null) ? this.sport = sport : this.sport = null;
	}

	signup(user): Observable<any> {
		const params = JSON.stringify(user);

		return this._http.post(this.url + 'login', params, {headers: this.headers});
	}

	login(): Observable<any> {	
		return this._http.get(this.url + 'login/hash', {headers: this.headerAuth()});
	}

	register(user): Observable<any> {
		const url = this.url + 'register';
		const params = JSON.stringify(user);		

		return this._http.post(url, params, {headers: this.headerAuth()});
	}

	createUser(user): Observable<any> {
		user.club = this.getIdentity().club.id;
		user.sport_id = this.getSport();
		let params = JSON.stringify(user);

		return this._http.post(this.url + 'users', params, {headers: this.headerAuth()});
	}

	updateUser(user): Observable<any> {
		const url = this.url + 'users/' + user.id;

		if (user.email === this.getIdentity().email) {
			user.email = undefined;
		}
		const params = JSON.stringify(user);

		return this._http.post(url, params, {headers: this.headerAuth()});
	}

	getUser(id: number): Observable<any> {
		return this._http.get(this.url + 'users/' + id, {headers: this.headerAuth()});
	}

	getUsersList(role: string, team = false): Observable<any> {
		let query = '?role=' + role;
		query += '&sport=' + this.getSport();
		query += '&club=' + this.getIdentity().club.id;
		if (team) { query += '&team=' + team; }

		return this._http.get(this.url + 'users' + query, {headers: this.headerAuth()})
			.pipe(
				retry(3)
			);
	}

	addTeam(userId, teamId): Observable<any> {	
		const url = this.url + 'users/' + userId + '/add-team';
		let params = { 'team_id': teamId }

		return this._http.post(url, params, {headers: this.headerAuth()});
	}

	removeTeam(userId, teamId): Observable<any> {
		const url = this.url + 'users/' + userId + '/remove-team';
		let params = { 'team_id': teamId }

		return this._http.post(url, params, {headers: this.headerAuth()});
	}

	isAuth() {
		return (this.getToken() != null) ? true : false;
	}

	logout(): void {
		this.identity = null;
		this.token = null;
		this.sport = null;
		localStorage.removeItem('identity');
		localStorage.removeItem('token');
		localStorage.removeItem('sport');
		window.location.href = '/login';		
	}

	private handleError2(errorResponse: HttpErrorResponse) {
		if (errorResponse instanceof ErrorEvent) {
			console.error('Client side error', errorResponse.error);
		} else {
			console.error('Server side error', errorResponse);
		}
	}

	private handleError<T> (operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
	   
		  // TODO: send the error to remote logging infrastructure
		  console.error(operation, error); // log to console instead
	   
		  // TODO: better job of transforming error for user consumption
		  console.log(`${operation} failed: ${error.message}`);
	   
		  // Let the app keep running by returning an empty result.
		  return error;
		};
	  }

}
