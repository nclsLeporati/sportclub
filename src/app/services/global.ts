export const GLOBAL = {
	// url: 'http://localhost:8080/SymApp/sportclub-api/web/app_dev.php/'
	// url: 'http://localhost:8080/SymApp/symfony/web/app_dev.php/'
	// url: 'http://localhost:80/laravel/public/api/',
	url: 'https://club-app-fdf60.herokuapp.com/api/',

	// storage: 'http://localhost:80/laravel/public/storage/'
	// storage: 'https://club-app-fdf60.herokuapp.com/storage/'
	storage: 'https://clubapp-storage.000webhostapp.com/storage'

};
