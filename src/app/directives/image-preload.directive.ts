import {Directive, Input, HostBinding} from '@angular/core'

@Directive({
    selector: 'img[default]',
    host: {
      '(error)':'updateSrc()',
      // '(load)': 'load()',
      '[src]':'src'
     }
  })

 export class ImagePreloadDirective {
    @Input() src:string;
    @Input() default:string;
    // @HostBinding('class') className

    updateSrc() {
      this.src = this.default;
    }

    // load(){
    //   this.className = 'image-loaded';
    // }
  }
